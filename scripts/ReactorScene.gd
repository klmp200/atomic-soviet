extends Spatial

	
func _ready():
	pass
	
func reset():
	get_node("Camera/ProgressBar").reset()
	get_node("Camera/ScenePreview").reset()
	get_node("Reactor").reset()
	get_node("Camera/RightPanel/VBoxContainer/BudgetBar").reset()
	get_node("Camera/RightPanel").reset()
	get_node("Camera/RichTextLabel").reset()