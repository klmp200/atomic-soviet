extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var sold_item
var scene_manager

func init(item):
	sold_item = item
	self.rect_min_size = Vector2(100,100)
	scene_manager = get_tree().get_root().get_node("SceneManager")
	self.text = item.name + "         " + str(sold_item.cost)
	
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
func _pressed():
	if scene_manager != null:
		if scene_manager.player.budget >= sold_item.cost:
			scene_manager.player.supply[sold_item] += 1
			sold_item.update_label()
			scene_manager.player.budget -= sold_item.cost
			scene_manager.get_node("Pause/RightPanel/VBoxContainer/BudgetBar").update_value()
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass