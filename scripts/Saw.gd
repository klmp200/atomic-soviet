extends "Tool.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var cost = 30

func _ready():
	efficiencies = {
			"Cables" : 10,
			"Nuclear_addon" : 4,
			"Top_pipe" : 8,
			"Side_pipe" : 8,
			"Filter" : 6,
			"Window" : 0
	}

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
