extends Node2D

var label = "Pause"

func reset():
	get_node("RightPanel").reset()
	get_node("ScenePreview").reset()