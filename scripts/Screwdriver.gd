extends "Tool.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var cost = 10

func _ready():
	efficiencies = {
			"Cables" : 2,
			"Nuclear_addon" : 7,
			"Top_pipe" : 3,
			"Side_pipe" : 3,
			"Filter" : 6,
			"Window" : 7
	}

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
