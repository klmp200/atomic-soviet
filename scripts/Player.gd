var tool_ressources = preload("res://scenes/Tool.tscn")
var glove_ressources = preload("res://scenes/Glove.tscn")
var grinder_ressources = preload("res://scenes/Grinder.tscn")
var pied_biche_ressources = preload("res://scenes/PiedBiche.tscn")
var saw_ressources = preload("res://scenes/Saw.tscn")
var screwdriver_ressources = preload("res://scenes/Screwdriver.tscn")
var suit_ressources = preload("res://scenes/Suit.tscn")
var shoes_ressources = preload("res://scenes/Shoes.tscn")


var cancer
var budget
var supply = {
	glove_ressources.instance() : 2,
	grinder_ressources.instance() : 4,
	pied_biche_ressources.instance() : 4,
	saw_ressources.instance() : 1,
	screwdriver_ressources.instance() : 2,
	suit_ressources.instance() : 1,
	shoes_ressources.instance() : 2
}

func _init():
	cancer = 0
	budget = 50