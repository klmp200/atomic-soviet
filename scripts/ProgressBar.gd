extends ProgressBar


var sceneTime
var elapsedTime

func _ready():
	sceneTime = 10
	elapsedTime = 0

func _process(delta):
	elapsedTime += delta
	self.value = (elapsedTime * 100)/sceneTime
	if self.value == 100:
		var scene_manager = get_tree().get_root().get_node("SceneManager")
		if scene_manager != null:
			scene_manager.next_scene()
			
func reset():
	self.value = 0
	self.elapsedTime = 0