extends "Tool.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var cost = 2

func _ready():
	efficiencies = {
			"Cables" : 1,
			"Nuclear_addon" : 7,
			"Top_pipe" : 1,
			"Side_pipe" : 2,
			"Filter" : 5,
			"Window" : 9
	}
	# Called every time the node is added to the scene.
	# Initialization here

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
