extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var scene_manager

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	scene_manager = get_tree().get_root().get_node("SceneManager")

func _pressed():
	if scene_manager != null:
		scene_manager.next_scene()
	else:
		print("Attention quand même, y'a un truc null")

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
