extends "Tool.gd"

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var cost = 17

func _ready():
	efficiencies = {
		"Cables" : 9,
		"Nuclear_addon" : 1,
		"Top_pipe" : 8,
		"Side_pipe" : 8,
		"Filter" : 4,
		"Window" : 0
	}
	# Called every time the node is added to the scene.
	# Initialization here

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
