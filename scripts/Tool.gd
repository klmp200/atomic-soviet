extends Container

var dragged
var initial_position
var efficiencies = {}
var scene_manager
var is_reactor_room


var supply_label = preload("res://scenes/SupplyLabel.tscn").instance()

func init(pos, scene_manager):
	is_reactor_room = (get_node("../..").name == "Camera")
	initial_position = pos
	rect_position = pos
	self.scene_manager = scene_manager
	if scene_manager != null:
		update_label()
	add_child(supply_label)

func _ready():
	pass

func _process(delta):
	if is_reactor_room:
		if get_global_mouse_position().distance_to(self.rect_global_position) < 40:
			if Input.is_mouse_button_pressed(1):
				if get_node("..").dragged_object == null: # .. = RightPanel 
					self.dragged = true
					get_node("..").dragged_object = self 
				
		if dragged:
			self.rect_global_position = get_global_mouse_position()
			
		if dragged and not(Input.is_mouse_button_pressed(1)):
			if scene_manager != null:
				var camera = scene_manager.get_node("ReactorScene/Camera")
				var drop_zone = camera.part_projected_on(get_viewport().get_mouse_position())
				if drop_zone != null:
					self.drop_on(drop_zone)
			else:
				print("ça marche mais t'es un peu un enculé")
			dragged = false
			get_node("..").dragged_object = null
			rect_position = initial_position if initial_position != null else rect_position

func update_label():
	supply_label.get_node("Text").text = str(scene_manager.player.supply[self])
	
func drop_on(zone):
	if scene_manager != null:
		if scene_manager.player.supply[self] > 0:
			zone.repair(efficiencies[zone.name])
			scene_manager.player.supply[self] -= 1
			update_label()
			