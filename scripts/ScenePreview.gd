extends Panel

var scene_manager

func _ready():
	scene_manager = get_tree().get_root().get_node("SceneManager")
	refresh_ui()
	
func reset():
	refresh_ui()

func refresh_ui():
	if scene_manager != null:
		var scene_preview = scene_manager.get_scene_preview()
		for i in range(0,3):
			get_node("HBoxContainer/Display" + str(i+1)).bbcode_text = "[center]" + str(scene_preview[i][1].label) + "[/center]"
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
