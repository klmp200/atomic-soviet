extends Camera


func _ready():
	pass

func _process(delta):
	var reactorPos = get_node("../Reactor").translation
	var newX = (cos(delta) * (self.translation.x - reactorPos.x) -
		sin(delta) * (self.translation.z - reactorPos.z))
	var newY = (sin(delta) * (self.translation.x - reactorPos.x) +
		cos(delta) * (self.translation.z - reactorPos.z))
	
	self.look_at_from_position(Vector3(newX, newY, self.translation.z), reactorPos,
		Vector3(0,1,0))
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.